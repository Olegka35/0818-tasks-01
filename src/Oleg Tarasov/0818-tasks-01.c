#include <stdio.h>
#include <locale.h>

#define _CRT_SECURE_NO_WARNINGS

int main()
{
	int rub = 0, cents = 0;
	setlocale(LC_ALL, "Russian");
	puts("Enter the sum in format dd.dd:");
	scanf_s("%d.%d", &rub, &cents);
	printf("%02d ���. %02d ���.\n", rub, cents);
	return 0;
}