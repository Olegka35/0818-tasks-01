#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <locale.h>
#include <math.h>

int main()
{
	setlocale(LC_ALL, "Russian");
	char str[256];
	int start = 0, sum = 0, val = 0;

	puts("������� ������:");
	fgets(str, 256, stdin);

	int i = 0;
	while(str[i] != '\0') {
		if (str[i] >= '0' && str[i] <= '9') {
			start = i;
			while (str[i] >= '0' && str[i] <= '9') 
				i++;
			val = 0;
			for (int j = start; j < i; j++) 
				val += (str[j] - '0') * (int)powf(10, i - j - 1);
			sum += val;
		}
		if(str[i] != 0) i++;
	}
	printf("����� ����� �� �������� ������: %d\n", sum);
	return 0;
}