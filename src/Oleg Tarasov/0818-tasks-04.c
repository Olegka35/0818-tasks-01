#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <locale.h>

int main()
{
	setlocale(LC_ALL, "Russian");
	char str[32];
	int size = 0, move = 0;

	puts("������� �����:");
	fgets(str, 32, stdin);
	for (int i = 0; str[i] != 0; i++) {
		if (!(str[i] >= '0' && str[i] <= '9')) {
			if (str[i] == '\n')
				str[i] = 0;
			else {
				puts("�������� ���� �����.");
				return 1;
			}
		}
		size = i;
		if (size > 20) {
			puts("�������� ���� ����� (�� ����� 20 ��������).");
			return 1;
		}
	}
	move = (size - 1) / 3;
	str[size + move] = 0;
	for (int i = size-1, digit = 0, group = 0; i >= 0; i--) {
		if (digit++ < 3)
			str[i + move] = str[i];
		else {
			str[i + move] = ' ';
			digit = 0;
			move--;
			i++; // ����������� i �� 1, ����������� ���������� �������� ������
		}
	}
	printf("�����: %s\n", str);
	return 0;
}