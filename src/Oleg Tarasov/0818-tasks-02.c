#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <locale.h>

int main()
{
	int inches = 0, feet = 0;
	float sm;
	setlocale(LC_ALL, "Russian");
	puts("������� �������� � �����-������ (������: ����.�����):");
	if (scanf("%d.%d", &feet, &inches) < 1) {
		puts("�������� ����.");
		return 0;
	}

	sm = (feet * 12 + inches) * 2.54f;
	printf("%02d.%02d ����� = %.2f �����������.\n", feet, inches, sm);
	return 0;
}